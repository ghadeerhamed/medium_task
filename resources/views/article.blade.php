@extends('layouts.app')

@push('head')
    <link href="{{ asset('css/create_article.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="">
                    <div class="text-center">
                        <h4 class="article-title text-capitalize text-left text-decoration-none font-weight-bold"> {{$article->title}} </h4>
                        <div class="text-right small font-italic">By {{$article->author->name}} :
                            <em>{{$article->created_at->format('M d, Y') .' - '. $article->created_at->diffForHumans()}}</em>
                        </div>
                    </div>
                </div>
                <div class="article-card">
                    <div class="card">
                        <div class="card-body text-secondary">

                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @foreach($article->images as $image)
                                        <div
                                            class="carousel-item item {!! isset($active) ? "" : "active"; $active= true; !!}">
                                            <img class="d-block w-100"
                                                 src="{{strpos($image->url, 'articles') !== false ? url($image->url) : url('img/articles/' . $image->url)}}"
                                                 alt="Article Image">
                                        </div>
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                   data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <div class="article-text">{!! $article->text !!}</div>
                            <div class="tags-card">
                                <ul class="tags-ul">
                                    @foreach($article->tags as $tag)
                                        <a class="d-inline-block btn btn-info border-success tag-item"
                                           href="{{route('article.filter_tag', $tag->name)}}"
                                           target="_blank">{{$tag->name}}</a>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @auth()
                            <div class="card-footer admin-buttons  border-top">
                                <a href="{{route('admin.articles.edit', $article->id)}}" target="_blank"
                                   class="btn btn-info">Update This Article</a>
                                <a onclick="delete_article({{$article->id}});" class="btn btn-danger float-right">Delete
                                    This Article</a>
                            </div>
                        @endauth
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
