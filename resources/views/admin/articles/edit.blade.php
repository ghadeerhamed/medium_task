@extends('layouts.app')

@push('head')
    <script src="{{ asset('js/create_article.js') }}" defer></script>
    <link href="{{ asset('css/create_article.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js" defer></script>
    <script src="//cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
@endpush

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="text-center">Update Article</div>
                    </div>

                    <form action="{{route('admin.articles.update', $article)}}" method="post"
                          enctype="multipart/form-data">
                        @method('PUT')
                        <div class="card-body">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input class="form-control" placeholder="Article Title" type="text"
                                       max="191" name="title" value="{{$article->title}}"
                                       id="title" required>
                            </div>

                            <div class="form-group">
{{--                                <label for="text">Article Paragraph</label>--}}
{{--                                <textarea class="form-control" name="text" id="text" rows="8"--}}
{{--                                          placeholder="Article Paragraph" required> {{$article->text}}</textarea>--}}
                                <label for="wysiwyg-editor">Article Paragraph</label>
                                <div class="form-group">
                                    <textarea class="ckeditor form-control" name="wysiwyg-editor">{!! $article->text !!}</textarea>
                                </div>
                            </div>

                            <div class="form-group m-auto" id="select-tags">
                                <label for="choices-multiple-tags">Tags</label>
                                <select class="form-control" name="tags[]" id="choices-multiple-tags" multiple
                                        name="tags[]" required>
                                    @foreach($tags as $tag)
                                        <option
                                            value="{{$tag->id}}" {{in_array($tag->id, $article_tags) ? "selected" :""}}>{{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!-- Existing Images -->
                            <label class="d-block mt-3" for="#">Images</label>
                            <div class="images-collection d-inline-flex row">
                                @foreach($article->images as $image)

                                    <div class="img-wrap">
                                        <span class="close article_img_del"><i class="fas fa-trash" id="{{$image->id}}"></i></span>
                                        <img alt="Image" width="200" src="{{strpos($image->url, 'articles') !== false ? url($image->url) : url('img/articles/' . $image->url)}}" />
                                    </div>
                                @endforeach
                            </div>
                            <!-- Images upload -->
                            <div class="form-group mt-4">
                                <div class="row">
                                    <div class="col-sm-2 col-md-3 imgUp">
                                        <div class="imagePreview"></div>
                                        <label class="btn btn-primary">
                                            Upload <input type="file" name="images[]" class="uploadFile img"
                                                          value="Upload Photo"
                                                          style="width: 0px;height: 0px;overflow: hidden;">
                                        </label>
                                    </div><!-- col-2 -->
                                    <i class="fa fa-plus imgAdd"></i>
                                </div><!-- row -->
                            </div><!-- container -->

                        </div>
                        <div class="card-footer border-top">
                            <button type="submit" class="btn btn-info btn-dark">Update</button>
                            <button onclick="history.back()" class="btn btn-danger float-right">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
