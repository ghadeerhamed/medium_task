@extends('layouts.app')
@push('head')
    <link href="{{ asset('css/create_article.css') }}" rel="stylesheet">
@endpush
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card"> <!--card -->
                    <div class="text-center card-header"> <!--card header-->
                        <h2>Latest Articles </h2>
                        <small
                            class="border-info">{{ isset($filter_tag) ? " Filter tag (". $filter_tag. ")" : ""}}</small>
                    </div>
                </div>


                @foreach($articles as $article)
                    <div class="article-card border-bottom">
                        <div class="card"><!--card-->
                            <div class="card-header text-left text-capitalize">
                                <a class="text-decoration-none font-weight-bold"
                                   href="{{route('article.show', $article->id)}}" target="_blank">
                                    {{$article->title}}
                                </a>
                            </div>

                            <div class="text-right small font-italic">By {{$article->author->name}} :
                                <em>{{$article->created_at->format('M d, Y') .' - '. $article->created_at->diffForHumans()}}</em>
                            </div>

                            @if($article->images()->count())
                                @php
                                    $img_url = $article->images()->first()->url
                                @endphp
                                <div class="article-img">
                                    <img
                                        src="{{strpos($img_url, 'articles') !== false ? url($img_url) : url('img/articles/' . $img_url)}}"
                                        alt="Main Article Image"/>
                                </div>
                            @endif
                            <div class="card-body text-secondary">
                                <div onclick="show_article({{$article->id}})" class="article-text">
                                    {!! $article->preview_text !!}...
                                </div>
                                <div class="tags-card">
                                    <ul class="tags-ul">
                                        @foreach($article->tags as $tag)
                                            <a class="d-inline-block btn btn-info border-success tag-item"
                                               href="{{route('article.filter_tag', $tag->name)}}"
                                               target="_blank">{{$tag->name}}</a>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @auth()
                                {{--                                <div class="card-footer admin-buttons">--}}
                                {{--                                    <a href="{{route('admin.articles.edit', $article->id)}}" target="_blank"--}}
                                {{--                                       class="btn btn-info">Update This Article</a>--}}
                                {{--                                    <a onclick="delete_article({{$article->id}});" class="btn btn-danger float-right">Delete--}}
                                {{--                                        This Article</a>--}}
                                {{--                                </div>--}}
                            @endauth
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection
