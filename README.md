
> **repo url**
    https://gitlab.com/ghadeerhamed/medium_task


> **install**<br>
1. **Clone the repo:**<br>
     `git clone https://gitlab.com/ghadeerhamed/medium_task.git`
1. **Open terminal [inside cloned project folder] and execute:** <br><br>
     - <code>composer install</code>  <em>Install packages</em><br>
     - <code>cp .env.example .env</code> <em>copy environment file</em><br>
 
     
1. **Database prepare:**<br>
     -  If you wish to use <b>sqlite</b> driver you have to create <code>database.sqlite</code> file in <code>database</code> folder<br>
     -  If you wish to use <b>mysql</b> driver you have to create empty database manually first.
     [database name must equal to what you provide in .env file in DB_DATABASE attribute].
     You also should change DB_CONNECTION attribute to 'mysql'.
 
1. **Execute in terminal**: <br>
    ****<code>php artisan key:generate</code> <em>Generate app key</em><br><br>
 
1. **Migration:** <br>
    Run command: <code>php artisan migrate --seed</code><br>
     <em>This will take around two minutes.</em>
    


> **Usage**

Run <code>php artisan serve</code> to start the development server.<br>
Go to http://localhost:8000/ to view the app.<br>

<hr>

<h3>Home page</h3> will list the articles ordered by updated date [desc].
Each article will have a title, author, created date, tags and may have images.
In the listing page:<br>
    <b>if</b> the article has images, We show the first one only.
    Article content (text) will not be fully displayed in listing page.

When pressing on any tag under the article, It will open new page listing all articles related to the tag.

<h3>Article page</h3>
When clicking article title or article content, It will open Article page in new tap to view full article text and images.<br><br>

> **Admin Pages**


In home page Header you can see the login button, This will take you to login page.

Enter the following credentials to login:<br>
 <b>Email:</b> <code>admin@admin.com</code><br>
 <b>Password:</b> <code>secret</code><br>
 
Once you are logged in, You can see the <b>Actions</b> button in the header.
Through that you can access the <b>create article</b> page.

<h3>Create article page</h3>
 Here you can post new article by filling the fields [Title, Paragraph, Tags, Images].
 
 - [ ] The Title field 'required' and represent the article title.<br>
- [ ]  The paragraph field 'required' and represent the article text, You can type formatted text in this field, And it will be shown to the visitors as you type it.<br>
- [ ]  The tags field 'required', Here you can select from previews tags from the database or type new tags and press 'Enter' to add new tag.<br>
- [ ]  Images field 'optional', Here you can add as many images as you want, Or skip it if your article doesn't have images.

<h3>Update article page</h3>
 -From the 'show article' page, If you are logged in, Two buttons will appear under the article.
  First button is 'Update This Article', 
  This will open a new tap to edit the article.<br>
  This page contains same fields as 'Create Article' page.<br>
  You an edit any field, Delete images and add new images. Then press 'Update' to save changes.
  
<h3>Delete Article</h3>
  From the 'show article' page, the second button under the article will delete it. [Be careful, Deleting an article can't be undone.]
  

 
 

