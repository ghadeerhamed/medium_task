<?php


namespace App\Repositories\Admin;


use App\Exceptions\GeneralException;
use App\Models\Image;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class ImagesRepository extends BaseRepository
{
    private $modelName = 'Image';


    public function __construct(Image $model)
    {
        $this->model = $model;
    }


    public function create(array $data): Image
    {
        return DB::transaction(function () use ($data) {
            $image = $this->model::create($data);

            if ($image) {
                return $image;
            }

            throw new GeneralException("There was a problem creating this $this->modelName. Please try again.");
        });
    }


    public function update(Image $image, array $data): Image
    {
        if ($image->update($data)) {
            return $image;
        }

        throw new GeneralException("There was a problem updating this $this->modelName. Please try again.");

    }
}
