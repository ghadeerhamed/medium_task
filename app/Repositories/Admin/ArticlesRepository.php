<?php


namespace App\Repositories\Admin;


use App\Exceptions\GeneralException;
use App\Models\Article;
use App\Models\Image;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ArticlesRepository extends BaseRepository
{
    private $modelName = 'Article';
    protected $tagsRepository;
    protected $imagesRepository;


    public function __construct(Article $model, TagsRepository $tagsRepository, ImagesRepository $imagesRepository)
    {
        $this->model = $model;
        $this->tagsRepository = $tagsRepository;
        $this->imagesRepository = $imagesRepository;
    }


    public function create(array $data, array $tags, array $images): Article
    {
        $data["text"] = $data["wysiwyg-editor"];
        unset($data["wysiwyg-editor"]);
        return DB::transaction(function () use ($data, $tags, $images) {
            $tags = $this->tagsRepository->addNewTags($tags);
            $data = array_merge($data, ['user_id' => Auth::id()]);
            $article = $this->model::create($data);
            if ($article) {
                $article->tags()->saveMany($this->tagsRepository->whereIn('tags.id', $tags)->get());
                foreach ($images as $image) {
                    $article->images()->save(new Image($image));
                }
                return $article;
            }

            throw new GeneralException("There was a problem creating this $this->modelName. Please try again.");
        });
    }


    public function update(Article $article, array $data, array $tags, array $images): Article
    {
        $data["text"] = $data["wysiwyg-editor"];
        unset($data["wysiwyg-editor"]);
        if ($article->update($data)) {
            $tags = $this->tagsRepository->addNewTags($tags);
            $article->tags()->sync($tags);
            foreach ($images as $image) {
                $article->images()->save(new Image($image));
            }
            return $article;
        }

        throw new GeneralException("There was a problem updating this $this->modelName. Please try again.");

    }

    public function deleteById($id)
    {
        $article = $this->getById($id);
        foreach ($article->images as $image) {
            $image_url = strpos($image->url, 'articles') !== false ? $image->url : 'img/articles/' . $image->url;
            unlink(public_path($image_url));
        }
        $article->images()->delete();
        $article->tags()->detach();
        return $article->delete();
    }

    public function byTag(string $tag)
    {
        return $this->model->whereHas('tags', function ($q) use ($tag) {
            $q->where('name', 'like', $tag);
        })->orderBy('updated_at', 'desc')->get();
    }
}
