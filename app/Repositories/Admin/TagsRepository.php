<?php


namespace App\Repositories\Admin;


use App\Exceptions\GeneralException;
use App\Models\Tag;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class TagsRepository extends BaseRepository
{
    private $modelName = 'Tag';


    public function __construct(Tag $model)
    {
        $this->model = $model;
    }


    public function create(array $data): Tag
    {
        return DB::transaction(function () use ($data) {
            $tag = $this->model::create($data);

            if ($tag) {
                return $tag;
            }

            throw new GeneralException("There was a problem creating this $this->modelName. Please try again.");
        });
    }


    public function update(Tag $tag, array $data): Tag
    {
        if ($tag->update($data)) {
            return $tag;
        }

        throw new GeneralException("There was a problem updating this $this->modelName. Please try again.");

    }

    public function addNewTags(array $tags)
    {
        $new_tags_names = array_filter($tags, function ($element) {
            return !is_numeric($element);
        });
        $tags_ids = array_filter($tags, function ($element) {
            return is_numeric($element);
        });
        foreach ($new_tags_names as $name) {
            $tags_ids[] = $this->model::firstOrCreate(['name' => $name])->id;
        }
        return $tags_ids;
    }
}
