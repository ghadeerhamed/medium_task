<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResourceHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateArticleRequest;
use App\Http\Requests\updateArticleRequest;
use App\Repositories\Admin\ArticlesRepository;
use App\Repositories\Admin\ImagesRepository;
use App\Repositories\Admin\TagsRepository;
use Illuminate\Support\Facades\DB;

class ArticlesController extends Controller
{

    protected $articlesRepository;
    protected $tagsRepository;
    protected $imagesRepository;

    public function __construct(ArticlesRepository $articlesRepository, TagsRepository $tagsRepository, ImagesRepository $imagesRepository)
    {
        $this->articlesRepository = $articlesRepository;
        $this->tagsRepository = $tagsRepository;
        $this->imagesRepository = $imagesRepository;
    }


    public function create()
    {
        $tags = $this->tagsRepository->all();
        return view('admin.articles.create', compact('tags'));
    }


    public function store(CreateArticleRequest $request)
    {
        try {
            $images = ResourceHelper::upload_all_image($request);
            $this->articlesRepository->create($request->only(['title', 'wysiwyg-editor']), $request->get('tags'), $images);
            return redirect()->route('home')->withFlashSuccess("Article Created Successfully");
        } catch (\Exception $exception) {
            return back()->withInput()->withFlashDanger($exception->getMessage());
        }
    }


    public function edit($id)
    {
        $article = $this->articlesRepository->getById($id);
        $tags = $this->tagsRepository->all();
        $article_tags = $article->tags()->pluck('tags.id')->toArray();

        return view('admin.articles.edit', compact('article', 'tags', 'article_tags'));
    }


    public function update(updateArticleRequest $request, $id)
    {

        try {
            $images = ResourceHelper::upload_all_image($request);
            $article = $this->articlesRepository->getById($id);
            $this->articlesRepository->update($article, $request->only(['title', 'wysiwyg-editor']), $request->get('tags'), $images);
            return redirect()->route('home')->withFlashSuccess("Article Updated Successfully");
        } catch (\Exception $exception) {
            return back()->withInput()->withFlashDanger($exception->getMessage());
        }
    }


    public function destroy($id)
    {
        try {
            $deleted = $this->articlesRepository->deleteById($id);
            return response()->json(['message' => $deleted ? 'Article deleted!' : 'Failed to delete.']);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()]);
        }
    }

    public function delete_img($image_id)
    {
        $image = $this->imagesRepository->getById($image_id);
        $image_url = strpos($image->url, 'articles') !== false ? $image->url : 'img/articles/' . $image->url;
        if (unlink(public_path($image_url))) {
            $image->delete();
            return response()->json(['message' => 'Success']);
        }
        return response()->json(['message' => 'Failed'], 500);
    }
}
