<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\Admin\ArticlesRepository;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    protected $articlesRepository;

    public function __construct(ArticlesRepository $articlesRepository)
    {
        $this->articlesRepository = $articlesRepository;
    }


    public function index()
    {
        $data = $this->articlesRepository->orderBy('updated_at', 'desc')->get();

        return view('home', ['articles' => $data]);
    }


    public function show(int $id)
    {
        $article = $this->articlesRepository->with(['tags', 'images'])->getById($id);

        return view('article', compact('article'));
    }

    public function filter_tag(string $tag)
    {
        $data = $this->articlesRepository->byTag($tag);

        return view('home', ['articles' => $data , 'filter_tag' => $tag]);
    }

}
