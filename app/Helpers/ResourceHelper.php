<?php


namespace App\Helpers;


use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ResourceHelper
{

    public static function upload_image(UploadedFile $file, $folder)
    {

        $path = public_path('img/' . $folder);
        if (!is_dir($path))
            mkdir($path, 0777, true);
        $name = uniqid() . '_' . time() . '.' . $file->getClientOriginalExtension();
        try {

            $jpg = Image::make($file->getRealPath())->encode($file->getClientOriginalExtension(), 100)->heighten(480)->widen(640);
            $jpg->save(public_path("/img/$folder/$name"));
            return ['url' => "/img/$folder/$name"];
        } catch (FileException $exception) {
            throw new FileException($exception->getMessage());
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }


    public static function upload_all_image(Request $request, string $folder = 'articles')
    {
        $images = [];
        if (!$request->files->count())
            return $images;
        try {
            foreach (request()->files->get('images') as $file) {
                $images[] = self::upload_image($file, $folder);
            }
            return $images;
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }
}
