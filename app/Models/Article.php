<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Article
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property-read User|Model $author
 * @property-read Tag[]|Collection $tags
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @method static Builder| Article newModelQuery()
 * @method static Builder| Article newQuery()
 * @method static Builder| Article query()
 * @method static Builder| Article whereCreatedAt($value)
 * @method static Builder| Article whereId($value)
 * @method static Builder| Article whereTitle($value)
 * @method static Builder| Article whereUserId($value)
 *
 */
class Article extends Model
{
    protected $guarded = ['id'];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'articles_tags');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function getPreviewTextAttribute()
    {
        $wrapped = wordwrap($this->text, strlen($this->text) / 5, "wrap_break_str", true);

        $to = strpos($wrapped, "wrap_break_str");
        return substr($wrapped, 0, $to);
    }
}
