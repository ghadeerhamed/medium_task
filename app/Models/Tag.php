<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


/**
 * Class Tag
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property-read Article[]|Collection $articles
 *
 * @method static Builder| Article newModelQuery()
 * @method static Builder| Article newQuery()
 * @method static Builder| Article query()
 * @method static Builder| Article whereId($value)
 * @method static Builder| Article whereName($value)
 *
 */
class Tag extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'articles_tags');
    }
}
