<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/test', function () {
    return phpinfo();
});

//Auth::routes();
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'ArticlesController@index')->name('home');
Route::get('/articles/{article}', 'ArticlesController@show')->name('article.show');
Route::get('/articles/tagged/{tag}', 'ArticlesController@filter_tag')->name('article.filter_tag');


Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['web', 'auth']], function ($q) {
    Route::resource('articles', 'Admin\ArticlesController')->except(['index', 'show']);
    Route::post('/images/{image_id}/delete', 'Admin\ArticlesController@delete_img')->name('images.delete');
});


Route::any('{catchall}', function (){
    return redirect()->route('home')->setStatusCode(302);
})->where('catchall', '.*');
