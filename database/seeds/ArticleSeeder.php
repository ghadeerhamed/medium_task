<?php

use App\Models\Article;
use App\Models\Image;
use App\Models\Tag;
use Illuminate\Database\Seeder;


class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        factory(Article::class, 30)->create()->each(function ($a) use ($faker) {
            /** @var Article $a */
            $a->tags()->saveMany(Tag::query()->inRandomOrder()->limit(rand(3, 10))->get());
            $r = rand(1, 3);
            for ($i = 0; $i < $r; $i++) {
                $image = $faker->image('public/img/articles', 640, 480, null, false);
                $a->images()->save(new Image(['url' => $image]));
            }
        });
    }
}
