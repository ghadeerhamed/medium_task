<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    if (!is_dir(public_path('img')))
        mkdir(public_path('img'));
    if (!is_dir(public_path('img/articles')))
        mkdir(public_path('img/articles'));

    return [
        'title' => $faker->sentence,
        'text' => $faker->paragraph(rand(20, 40)),
        'user_id' => 1,
    ];
});
